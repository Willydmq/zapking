const express = require("express");
const mongoose = require("mongoose");
const { connectDB } = require("./services/services");
const routes = require("./routes/routes");
const server = express();
const cors = require("cors");
require("dotenv").config();
const PORT = process.env.PORT;

server.use(cors())
server.use(express.json());

server.get("/", (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "true");
  res.send("Api is running...");
});

//routes
server.use(routes);

//Iniciar el servidor
server.listen(PORT, () => {
  connectDB();
  console.log(`Server listening on port ${PORT}`);
});
