const { Router } = require("express");
const router = Router();
const {
  findUsers,
  findProducts,
  getUserById,
  getUserSpecialPrice,
  getDefaultProductPrice,
  createProduct,
  createUser,
  UserById,
  ProductByName,
  deleteUserById,
  deleteProductByName,
} = require("../services/services");
const { validateUserPost } = require("../middlewares/users.post");
const { validateUserPatch } = require("../middlewares/users.patch");
const { validateProductPost } = require("../middlewares/products.post");
const { validateProductPatch } = require("../middlewares/products.patch");

/**--------------*USER*-------------- */

// Endpoint para obtener los usuarios
router.get("/users", async (req, res) => {
  try {
    const users = await findUsers();
    res.json(users);
  } catch (error) {
    console.error(error);
    res.status(500).send("Error getting users");
  }
});

// Endpoint para crear los usuarios
router.post("/users", validateUserPost, async (req, res) => {
  try {
    const product = await createUser(req.body);

    res.status(201).json(product);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Endpoint para actualizar los usuarios
router.patch("/users/:userId", validateUserPatch, async (req, res) => {
  try {
    const { userId } = req.params;

    const updatedUser = await UserById(userId, req.body);

    if (!updatedUser) {
      return res.status(404).json({ message: "User not found" });
    }

    res.json(updatedUser);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Endpoint para eliminar los usuarios
router.delete("/users/:userId", async (req, res) => {
  try {
    const { userId } = req.params;

    const deletedUser = await deleteUserById(userId);

    if (!deletedUser) {
      return res.status(404).json({ message: "User not found" });
    }

    res.json({ message: "User deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

/**--------------*PRODUCT*-------------- */

// Endpoint para obtener los productos
router.get("/products", async (req, res) => {
  try {
    const products = await findProducts();
    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).send("Error getting products");
  }
});

// Endpoint para crear los productos
router.post("/products", validateProductPost, async (req, res) => {
  try {
    const product = await createProduct(req.body);

    res.status(201).json(product);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Endpoint para actualizar los productos
router.patch(
  "/products/:productName",
  validateProductPatch,
  async (req, res) => {
    try {
      const { productName } = req.params;

      const updatedProduct = await ProductByName(productName, req.body);

      if (!updatedProduct) {
        return res.status(404).json({ message: "Product not found" });
      }

      res.json(updatedProduct);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  }
);

// Endpoint para eliminar los product
router.delete("/products/:productName", async (req, res) => {
  try {
    const { productName } = req.params;

    const deletedProduct = await deleteProductByName(productName);

    if (!deletedProduct) {
      return res.status(404).json({ message: "Product not found" });
    }

    res.json({ message: "Product deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

/**--------------*PRICE*-------------- */

// Endpoint para obtener los precios
router.get("/price/:userId/:productName", async (req, res) => {
  const { userId, productName } = req.params;

  const user = await getUserById(userId);

  if (!user) {
    return res.status(404).send("User not found");
  }

  const userPrice = await getUserSpecialPrice(userId, productName);

  if (userPrice) {
    return res.json({
      product: userPrice.product_name,
      price: userPrice.special_price,
    });
  }

  const defaultPrice = await getDefaultProductPrice(productName);

  if (defaultPrice) {
    return res.json({
      product: defaultPrice.product_name,
      price: defaultPrice.price,
    });
  }

  res.status(404).send("Product not found");
});

module.exports = router;
