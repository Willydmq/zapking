const { body, validationResult } = require("express-validator");

const validateUserPost = [
  body("id").isInt({ min: 1 }).withMessage("ID must be a positive number"),

  body("name").isString().withMessage("Name must be a string"),

  body("special_prices")
    .isArray()
    .withMessage("Special prices must be an array"),

  body("special_prices.*.product_name")
    .isString()
    .withMessage("Product name must be a string"),

  body("special_prices.*.special_price")
    .isNumeric()
    .withMessage("Special price must be a number"),

  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

module.exports = {
  validateUserPost,
};
