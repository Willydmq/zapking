const { body, validationResult } = require("express-validator");

const validateProductPatch = [
  body("product_name")
    .isString()
    .withMessage("Name must be a string")
    .not()
    .isEmpty()
    .withMessage("Name is required"),

  body("price")
    .isNumeric()
    .withMessage("Price must be a number")
    .not()
    .isEmpty()
    .withMessage("Price is required"),

  body("stock")
    .isNumeric()
    .withMessage("Stock must be a number")
    .not()
    .isEmpty()
    .withMessage("Stock is required"),

  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

module.exports = {
  validateProductPatch,
};
