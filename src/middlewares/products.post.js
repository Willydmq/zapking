const { body, validationResult } = require("express-validator");

const validateProductPost = [
  body("product_name").isString().withMessage("Name must be a string"),

  body("price").isNumeric().withMessage("Special price must be a number"),

  body("stock").isNumeric().withMessage("Special price must be a number"),

  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

module.exports = {
  validateProductPost,
};
