const { body, validationResult } = require("express-validator");

const validateIdMatch = (req, res, next) => {
  if (req.body.id !== req.params.userId) {
    return res.status(400).json({ message: "Cannot update ID" });
  }

  next();
};

const validateUserPatch = [
  validateIdMatch,

  body("name")
    .isString()
    .withMessage("Name must be a string")
    .not()
    .isEmpty()
    .withMessage("Name is required"),

  body("special_prices")
    .isArray()
    .withMessage("Special prices must be an array")
    .not()
    .isEmpty()
    .withMessage("Special prices are required"),

  body("special_prices.*.product_name")
    .isString()
    .withMessage("Product name must be a string")
    .not()
    .isEmpty()
    .withMessage("Product name is required"),

  body("special_prices.*.special_price")
    .isNumeric()
    .withMessage("Special price must be a number")
    .not()
    .isEmpty()
    .withMessage("Special price is required"),

  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

module.exports = {
  validateUserPatch,
};
