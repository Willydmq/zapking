const mongoose = require("mongoose");
require("dotenv").config();

// Importar schemas
const User = require("../models/users.js");
const Product = require("../models/products.js");

// Conectar a MongoDB
const dbUrl = process.env.MONGO_URL;

async function connectDB() {
  try {
    await mongoose.connect(dbUrl);
    console.log("Connected to MongoDB");
  } catch (error) {
    console.error("Error connecting to MongoDB:", error);
  }
}
/**--------------*USER*-------------- */

async function createUser(data) {
  try {
    const existingUser = await User.findOne({ id: data.id });

    if (existingUser) {
      throw new Error("User already exists");
    }

    const user = new User(data);

    return await user.save();
  } catch (error) {
    console.error("Error creating user", error);
    throw error;
  }
}

async function findUsers() {
  try {
    return await mongoose.connection.db.collection("users").find().toArray();
  } catch (error) {
    console.error("Error getting users:", error);
    throw error;
  }
}

async function UserById(userId, userData) {
  try {
    const updatedUser = await User.findOneAndUpdate(
      { id: parseInt(userId) },
      userData,
      { new: true }
    );

    return updatedUser;
  } catch (error) {
    throw new Error(error);
  }
}

async function deleteUserById(userId) {
  try {
    const deletedUser = await User.findOneAndDelete({ id: parseInt(userId) });
    return deletedUser;
  } catch (error) {
    throw new Error(error);
  }
}

/**--------------*PRODUCT*-------------- */

async function createProduct(data) {
  try {
    const existingProduct = await Product.findOne({
      product_name: data.product_name,
    });

    if (existingProduct) {
      throw new Error("Product already exists");
    }

    const product = new Product(data);

    return await product.save();
  } catch (error) {
    console.error("Error creating product:", error);
    throw error;
  }
}

async function findProducts() {
  try {
    return await mongoose.connection.db.collection("products").find().toArray();
  } catch (error) {
    console.error("Error getting products:", error);
    throw error;
  }
}

async function ProductByName(productName, userData) {
  try {
    const { product_name } = userData;

    const existingProduct = await Product.findOne({ product_name });

    if (existingProduct && existingProduct.product_name !== productName) {
      throw new Error("Product name already exists");
    }

    const updatedProduct = await Product.findOneAndUpdate(
      { product_name: productName },
      userData,
      { new: true }
    );

    return updatedProduct;
  } catch (error) {
    throw new Error(error.message);
  }
}

async function deleteProductByName(productName) {
  try {
    const deletedProduct = await Product.findOneAndDelete({
      product_name: productName,
    });
    return deletedProduct;
  } catch (error) {
    throw new Error(error);
  }
}

/**--------------*PRICE*-------------- */

async function getUserById(userId) {
  return await mongoose.connection.db
    .collection("users")
    .findOne({ id: parseInt(userId) });
}

async function getUserSpecialPrice(userId, productName) {
  const user = await getUserById(userId);
  return user.special_prices.find((p) => p.product_name === productName);
}

async function getDefaultProductPrice(productName) {
  return await mongoose.connection.db
    .collection("products")
    .findOne({ product_name: productName });
}

module.exports = {
  connectDB,
  createUser,
  findUsers,
  UserById,
  deleteUserById,
  createProduct,
  findProducts,
  ProductByName,
  deleteProductByName,
  getUserById,
  getUserSpecialPrice,
  getDefaultProductPrice,
};
