const mongoose = require("mongoose");
const { Schema } = mongoose;

const userSchema = new Schema({
  id: { type: Number, required: true, unique: true },
  name: { type: String, required: true },
  special_prices: [
    {
      product_name: { type: String, required: true },
      special_price: { type: Number, required: true },
    },
  ],
});

const User = mongoose.model("User", userSchema);

module.exports = User;
