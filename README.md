# Nombre del Proyecto 📋

"**ZapKing:** encontrarás las mejores marcas de zapatillas para todos los gustos y presupuestos. Desde los clásicos urbanos más populares, hasta modelos exclusivos y ediciones limitadas.Nuestras membresías VIP te permiten acceder a descuentos especiales, lanzamientos anticipados y precios únicos en zapatillas premium. 😀

# Link Proyecto

<div style="display: flex; flex-direction: column; align-items: center;">
    <img src="./src/img/shoes.png" width="50px">
    <a style="color: blue; font-size: 20px; display: block; text-align: center;" href="https://zapking.onrender.com/users" target="_blank">Lista de Users "API"</a>
    <br>--</br>
    <a style="color: blue; font-size: 20px; display: block; text-align: center;" href="https://zapking.onrender.com/products" target="_blank">Lista de Products "API"</a>
</div>

## Instalación ⚙️

1. Asegúrese de tener instalado Node.js en su computadora. Si no lo tiene instalado, puede descargar e instalar **Node.js** en el sitio web oficial de Node.js.

2. Descargar o clonar el repositorio del proyecto desde GitLab.

3. Abrir una terminal o línea de comandos en la carpeta raíz del proyecto.

4. Ejecutar el comando npm install para instalar todas las dependencias necesarias del proyecto, incluyendo:

   - ![NPM](https://img.shields.io/badge/cors-NPM-red) <a href="https://www.npmjs.com/package/cors">cors</a>
   - ![NPM](https://img.shields.io/badge/dotenv-NPM-blue) <a href="https://www.npmjs.com/package/dotenv">dotenv</a>
   - ![NPM](https://img.shields.io/badge/express-NPM-green) <a href="https://www.npmjs.com/package/express">express</a>
   - ![NPM](https://img.shields.io/badge/expressvalidator-NPM-orange) <a href="https://www.npmjs.com/package/express-validator">express-validator</a>
   - ![NPM](https://img.shields.io/badge/mongoose-NPM-white) <a href="https://www.npmjs.com/package/mongoose">mongoose</a>

   ```
   npm install
   ```

   Una vez que hayas instalado las dependencias, puedes comenzar a configurar tu servidor de backend en Node.js utilizando Express. Deberás configurar la conexión a tu base de datos MongoDB utilizando mongoose.

   También puedes configurar las variables de entorno utilizando dotenv, que te permitirá almacenar información sensible como credenciales de la base de datos sin exponerlas en tu código.

5. **"A tener en cuenta":** Para probar la API, primero debes importar el archivo de Insomnia adjunto **"Drenvio_Insomnia.yaml"**. Esto te permitirá hacer requests DELETE, POST, PATCH y GET contra los endpoints de la API. Una vez importado, podrás comenzar a probar los diferentes métodos para crear, leer, actualizar y eliminar recursos. Verifica que las respuestas JSON sean las esperadas. Recuerda que debes tener el servidor Express ejecutándose para que la API responda a los requests ó interactuar con el Link del proyecto donde ya esta desplegado.[Users](https://zapking.onrender.com/users) y [Products](https://zapking.onrender.com/products)

6. Una vez completados los pasos anteriores, ejecutar el comando npm run dev para iniciar la aplicación.
   ```
   npm run dev
   ```
7. Acceder a la URL http://localhost:4000 en un navegador web para ver la aplicación en funcionamiento.

## Construido con 🛠️

<div style="text-align: center; padding: 10px;">
    <img src="./src/img/express.png" width="100px">
    <img src="./src/img/mongo.png" width="100px">
</div>

## Autores ✒️

- **William Maldonado** - _DRENVIO - NodeJS Challenge_ - [Willydmq](https://gitlab.com/Willydmq)

---

⌨️ con ❤️ por [William Maldonado](https://gitlab.com/Willydmq) 😊

---
